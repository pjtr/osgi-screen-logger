package net.luminis.screenlogger;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogService;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ScreenLogger {

    String title = "OSGi log service";
    List<LogEntry> entries = Collections.synchronizedList(new ArrayList<LogEntry>());

    JFrame frame;
    JTable table;
    AbstractTableModel model;
    Color backgroundEven = new Color(224, 255, 255);
    Color backgroundOdd = new Color(173, 216, 230);
    JLabel messageArea = new JLabel();

    int halfRowHeight;
    boolean locked;
    int lastRowSeen;

    public ScreenLogger()
    {
        model = new LogEntriesTableModel();
        table = new JTable(model) {
            @Override
            public String getToolTipText(MouseEvent mouseEvent) {
                int row = table.rowAtPoint(mouseEvent.getPoint());
                if (row >= 0 && row < entries.size() && entries.get(row).getException() != null) {
                    return "double-click to show stack trace";
                }
                else {
                    return null;
                }
            }

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column){
                Component locationComponent = super.prepareRenderer(renderer, row, column);
                if (!locationComponent.getBackground().equals(getSelectionBackground())){
                    locationComponent.setBackground(row % 2 == 0 ? backgroundOdd : backgroundEven);
                }
                return locationComponent;
            };
        };

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                int row = table.rowAtPoint(mouseEvent.getPoint());
                if (row >= 0 && row < entries.size()) {
                    if (mouseEvent.getClickCount() == 2) {
                        if (row < entries.size()) {
                            showException(entries.get(row));
                        }
                    }
                }
            }
        });

        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        table.setPreferredScrollableViewportSize(new Dimension(960, 170));
        scrollPane.getViewport().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                updateUnseenRowCount();
            }
        });

        int[] columnSizes = { 10, 130, 130, 130, 40, 240, 180 };
        for (int i = 0; i < columnSizes.length; i++) {
            table.getColumnModel().getColumn(i).setPreferredWidth(columnSizes[i]);
        }
        table.getColumnModel().getColumn(1).setCellRenderer(new DateRenderer());

        table.setRowHeight(table.getRowHeight() + 3);
        halfRowHeight = table.getRowHeight() / 2;

        JPanel contentPanel = new JPanel(new BorderLayout());
        int borderWidth = 5;
        contentPanel.setBorder(BorderFactory.createEmptyBorder(borderWidth, borderWidth, borderWidth, borderWidth));
        contentPanel.add(scrollPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 20, 0));
        messageArea = new JLabel();
        buttonPanel.add(messageArea);
        final JToggleButton lockButton = new JToggleButton("lock");
        buttonPanel.add(lockButton);
        JButton clearButton = new JButton("clear");
        buttonPanel.add(clearButton);
        contentPanel.add(buttonPanel, BorderLayout.SOUTH);
        clearButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (!entries.isEmpty() && JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(frame, "Discard all log entries?", "confirm", JOptionPane.OK_CANCEL_OPTION)) {
                    entries.clear();
                    model.fireTableDataChanged();
                }
            }
        });
        lockButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                locked = !locked;
                messageArea.setText("");
            }
        });
        buttonPanel.add(Box.createHorizontalStrut(15));
        frame = new JFrame(title);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                if (!entries.isEmpty()) {
                    String exitWarning = "Are you sure you want to close the " + title + " window?\nAll log entries will be lost. You need to restart the bundle to get a new window.";
                    int confirmed = JOptionPane.showConfirmDialog(null, exitWarning, title, JOptionPane.YES_NO_OPTION);
                    if (confirmed == JOptionPane.YES_OPTION)
                        dispose();
                }
                else {
                    dispose();
                }
            }
        });
        frame.getContentPane().add(contentPanel);
        frame.pack();
        frame.setVisible(true);
    }

    public void dispose() {
        frame.setVisible(false);
        frame.dispose();
    }

    public LogService createLogService(Bundle bundle) {
        return new LogServiceImpl(bundle);
    }

    private synchronized void log(LogEntry logEntry) {
        entries.add(logEntry);
        final int number = entries.size();
        model.fireTableRowsInserted(number-1, number);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (locked) {
                    updateUnseenRowCount();
                }
                else {
                    table.scrollRectToVisible(table.getCellRect(number - 1, 0, true));
                    lastRowSeen = number - 1;
                }

            }
        });
    }

    private void showException(LogEntry entry) {
        Throwable exception = entry.getException();
        if (exception == null) {
            return;
        }

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        exception.printStackTrace(new PrintStream(buffer));

        final JDialog popup = new JDialog(frame, "Exception stacktrace", true);
        JTextArea stackTracePanel = new JTextArea();
        stackTracePanel.setEditable(false);
        stackTracePanel.setText(buffer.toString());

        int borderWidth = 2;
        JPanel centerPanel = new JPanel(new BorderLayout(0, 0));
        centerPanel.setBorder(BorderFactory.createEmptyBorder(borderWidth, borderWidth, 0, borderWidth));
        centerPanel.add(new JScrollPane(stackTracePanel));
        JPanel contentPanel = new JPanel(new BorderLayout(0, 0));
        contentPanel.add(centerPanel);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));

        JButton closeButton = new JButton("close");
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                popup.dispose();
            }
        });
        buttonPanel.add(closeButton);
        contentPanel.add(buttonPanel, BorderLayout.SOUTH);

        popup.getRootPane().setDefaultButton(closeButton);
        popup.add(contentPanel);
        popup.pack();
        popup.setVisible(true);
    }

    private String levelString(int level)
    {
        switch (level) {
            case LogService.LOG_DEBUG: return "Debug";
            case LogService.LOG_INFO: return "Info";
            case LogService.LOG_WARNING: return "Warning";
            case LogService.LOG_ERROR: return "Error";
            default: return "";
        }
    }

    private String convertToString(ServiceReference ref)
    {
        if (ref != null) {
            String serviceClass = "";
            if (ref.getProperty("objectClass") != null) {
                Object classes = ref.getProperty("objectClass");
                if (classes instanceof Object[]) {
                    classes = Arrays.asList(((Object[]) classes));
                }
                if (classes instanceof Iterable) {
                    for (Object element : (Iterable) classes) {
                        serviceClass += (serviceClass.length() > 0 ? ", " : "") + element.toString();
                    }
                } else {
                    serviceClass = classes.toString();
                }
            } else {
                serviceClass = ref.toString();
            }
            return serviceClass + " (registered by " + ref.getBundle().getSymbolicName() + ")";
        } else {
            return "";
        }
    }

    private String convertToString(Bundle bundle) {
        if (bundle != null) {
            return bundle.getSymbolicName() + " [" + bundle.getBundleId() + "]";
        }
        else {
            return "";
        }
    }

    private void updateUnseenRowCount() {
        Rectangle visibleRect = table.getVisibleRect();
        Point bottom = new Point(visibleRect.x, visibleRect.y + visibleRect.height - halfRowHeight);   // Consider row as seen when half of it is displayed
        int lastVisibleRow = table.rowAtPoint(bottom);
        if (lastVisibleRow == -1) {
            // i.e. all rows are visible
            lastVisibleRow = entries.size() - 1;       // lastVisibleRow is zero-based!
        }
        if (lastVisibleRow > lastRowSeen)
            lastRowSeen = lastVisibleRow;

        if (locked) {
            int newRows = entries.size() - lastRowSeen - 1;
            String msg = newRows > 0? "" + newRows + " new log entries": "";
            messageArea.setText(msg);
        }
        else {
            messageArea.setText("");
        }
    }

    class LogEntriesTableModel extends AbstractTableModel
    {
        private String[] columnNames = { "#", "time", "bundle", "service", "level", "message", "exception" };

        public String getColumnName(int col) {
            return columnNames[col].toString();
        }

        public int getRowCount() { return entries.size(); }

        public int getColumnCount() { return columnNames.length; }

        public Object getValueAt(int row, int col) {
            LogEntry entry = entries.get(row);
            switch (col) {
                case 0: return row + 1;
                case 1: return new Date(entry.getTime());
                case 2: return convertToString(entry.getBundle());
                case 3: return convertToString(entry.getServiceReference());
                case 4: return levelString(entry.getLevel());
                case 5: return entry.getMessage();
                case 6: return entry.getException() != null? entry.getException().toString(): "";
                default: return null;
            }
        }
    }

    static class LogEntryImpl implements LogEntry {

        Bundle bundle;
        ServiceReference serviceReference;
        int level;
        String message;
        long time;
        Throwable exception;

        LogEntryImpl(Bundle bundle, ServiceReference ref, long time, int level, String message, Throwable exception) {
            this.bundle = bundle;
            this.serviceReference = ref;
            this.time = time;
            this.level = level;
            this.message = message;
            this.exception = exception;
        }

        @Override
        public Bundle getBundle() {
            return bundle;
        }

        @Override
        public ServiceReference getServiceReference() {
            return serviceReference;
        }

        @Override
        public int getLevel() {
            return level;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public Throwable getException() {
            return exception;
        }

        @Override
        public long getTime() {
            return time;
        }
    }

    static class DateRenderer extends DefaultTableCellRenderer
    {
        DateFormat formatter;

        public DateRenderer() {
            super();
            formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        }

        public void setValue(Object value) {
            if (value instanceof Date) {
                setText((value == null) ? "" : formatter.format(value));
            }
            else if (value != null) {
                setText(value.toString());
            }
        }
    }

    public class LogServiceImpl implements LogService {

        Bundle sourceBundle;

        public LogServiceImpl(Bundle bundle) {
            sourceBundle = bundle;
        }

        @Override
        public void log(int level, String message) {
            log(null, level, message, null);
        }

        @Override
        public void log(int level, String message, Throwable exception) {
            log(null, level, message, exception);
        }

        @Override
        public void log(ServiceReference sr, int level, String message) {
            log(sr, level, message, null);
        }

        @Override
        public void log(ServiceReference sr, int level, String message, Throwable exception)
        {
            ScreenLogger.this.log(new LogEntryImpl(sourceBundle, sr, System.currentTimeMillis(), level, message, exception));
        }
    }

    public static void main(String[] args) {
        ScreenLogger screenLogger = new ScreenLogger();
        screenLogger.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        LogService logger = screenLogger.createLogService(null);
        logger.log(LogService.LOG_ERROR, "error!", new RuntimeException("something went wrong here..."));
        logger.log(LogService.LOG_WARNING, "you are warned!");
        logger.log(LogService.LOG_INFO, "this message is only informational");
        logger.log(LogService.LOG_DEBUG, "really, this is all just debug stuff");
    }

}
